package diaz.doriam;

import diaz.doriam.bl.entidades.Cita;
import diaz.doriam.bl.entidades.Mascota;
import diaz.doriam.bl.entidades.Persona;
import diaz.doriam.bl.entidades.Reservacion;

import java.io.PrintStream;
import java.util.Scanner;


import static diaz.doriam.bl.Bl.procesarOpcion;
import static diaz.doriam.ui.Ui.mostrarMenu;

public class Main {
    private static Scanner input = new Scanner(System.in);
    private static PrintStream output = new PrintStream(System.out);

    public static void main(String[] args) {
        // write your code here
        int opcion = 0;
        do {
            mostrarMenu();
            opcion = input.nextInt();
            procesarOpcion(opcion);
        } while (opcion != 7);
    }




}
