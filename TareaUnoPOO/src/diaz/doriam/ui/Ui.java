package diaz.doriam.ui;


import java.io.PrintStream;
import java.util.Scanner;

public class Ui {
    private static Scanner input = new Scanner(System.in);
    private static PrintStream output = new PrintStream(System.out);

    public static void mostrarMenu() {
        output.println("Bienvenido al sistema de registro, citas y reservaciones de la Veterinaria Moka");
        output.println("Este es el menú de opciones: ");
        output.println("1. Seleccione su tipo de usuario");
        output.println("2. Registrar a un nuevo cliente.");
        output.println("3. Registrar a la mascota.");
        output.println("4. Hacer una nueva cita.");
        output.println("5. Crear una reservación en el hotel de mascotas.");
        output.println("6. Imprimir listados de clientes, mascotas, citas o reservaciones.");
        output.println("7. Salir.");
        output.println("Ingrese su opción aquí --> ");
    }
}

