package diaz.doriam.bl;

import diaz.doriam.bl.entidades.*;

import java.io.PrintStream;
import java.util.Scanner;

public class Bl {
    private static Scanner input = new Scanner(System.in);
    private static PrintStream output = new PrintStream(System.out);
    private static Mascota[] arregloMascotas = new Mascota[20];
    private static Persona[] arregloPersonas = new Persona[20];
    private static Cita[] arregloCitas = new Cita[20];
    private static Reservacion[] arregloReservaciones = new Reservacion[20];
    private static int siguientePosicion = 0;
    private static boolean activo = true;

    public static void procesarOpcion(int opcion) {
        switch (opcion) {
            case 1:
                output.println("Escriba su nombre");
                String nombreUsuario = input.nextLine();
                output.println("Detalle su rol: doctor/a o enfermero/a");
                String rol = input.next();
                Usuario usuarioRegistrado = new Usuario(nombreUsuario, activo, rol);
                output.println(usuarioRegistrado);
                break;
            case 2:
                output.println("Por favor ingrese el nombre y apellidos del nuevo cliente: ");
                String nombreCompleto = input.nextLine();

                String cedula;

                boolean repetido = false;
                do {
                    output.println("Indique su número de cédula: ");
                    cedula = input.next();
                    for (int i = 0; i < arregloPersonas.length; i++) {
                        if (arregloPersonas[i] != null) {
                            if (arregloPersonas[i].getCedula().equals(cedula)) {
                                System.out.println("Ya existe esta cédula.");
                                System.out.println("Por favor vuelva a digitar la cédula.");
                                repetido = true;
                            } else {
                                System.out.println("Se incluyó la cédula.");
                                repetido = false;

                            }
                        }
                    }
                } while (repetido == true);
                output.println("Detalle el número de teléfono: ");
                int telefono = input.nextInt();
                output.println("Agregue los detalles de la dirección: ");
                String direccion = input.next();
                Persona nuevoCliente = new Persona(nombreCompleto, cedula, telefono, direccion);
                arregloPersonas[siguientePosicion] = nuevoCliente;
                siguientePosicion++;
                break;
            case 3:
                String nombreMascota;

                boolean repetida = false;
                do {
                    output.println("¿Cuál es el nombre de la mascota? ");
                    nombreMascota = input.next();
                    for (int i = 0; i < arregloMascotas.length; i++) {
                        if (arregloMascotas[i] != null) {
                            if (arregloMascotas[i].getNombreMascota().equals(nombreMascota)) {
                                System.out.println("Ya existe esta mascota en el sistema.");
                                System.out.println("Por favor vuelva a intentar con otro nombre.");
                                repetida = true;
                            } else {
                                System.out.println("Se incluyó correctamente la mascota.");
                                repetida = false;

                            }
                        }
                    }
                } while (repetida == true);

                output.println("Indique el nombre del dueño de la mascota: ");
                Duenno nombreDuenno = new Duenno(input.nextLine());
                output.println("Ingrese el nombre de la foto de la mascota: ");
                String foto = input.nextLine();
                output.println("¿Cuál es el ranking de la mascota (de 1 a 5, en que 1 es terrible y 5 excelente)? ");
                int ranking = input.nextInt();
                output.println("Detalle las observaciones especiales sobre la mascota: ");
                String observaciones = input.nextLine();
                Mascota ingreso = new Mascota(nombreMascota, nombreDuenno, foto, ranking, observaciones);
                arregloMascotas[siguientePosicion] = ingreso;
                siguientePosicion++;
                break;
            case 4:
                output.println("Ingrese el nombre de las mascota que será atendida en la cita: ");
                String mascotita = input.nextLine();
                output.println("Indique la fecha (dd/MM/yyyy) de la cita: ");
                String fecha = input.nextLine();
                output.println("Ingrese la hora de la cita: ");
                String hora = input.nextLine();
                output.println("¿Cuál es la razón de la cita? ");
                String observacion = input.nextLine();
                Cita citaNueva = new Cita(mascotita, fecha, hora, observacion);
                arregloCitas[siguientePosicion] = citaNueva;
                siguientePosicion++;
                break;
            case 5:
                output.println("Asigne un número de reservación: ");
                int numeroReservacion = input.nextInt();
                output.println("Indique el nombre de la mascota: ");
                String nombreAnimal = input.next();
                output.println("¿Cuál será el día de entrada de la mascota?");
                String diaEntrada = input.nextLine();
                output.println("¿Cuál será el día de salida de la mascota?");
                String diaSalida = input.nextLine();
                Reservacion reservacionAgregada = new Reservacion(numeroReservacion, nombreAnimal, diaEntrada, diaSalida);
                arregloReservaciones[siguientePosicion] = reservacionAgregada;
                siguientePosicion++;
                break;
            case 6:
                output.println("¿Qué lista desea imprimir: 1. Clientes. 2. Mascotas. 3. Citas. 4. Reservaciones? Escoja un número. ");
                int escogencia = input.nextInt();
                if (escogencia == 1) {
                    for (int i = 0; i < arregloPersonas.length; i++) {
                        output.println("#" + (i + 1) + " " + arregloPersonas[i]);
                    }
                }
                if (escogencia == 2) {
                    for (int i = 0; i < arregloMascotas.length; i++) {
                        output.println("#" + (i + 1) + " " + arregloMascotas[i]);
                    }
                }
                if (escogencia == 3) {
                    for (int i = 0; i < arregloCitas.length; i++) {
                        output.println("#" + (i + 1) + " " + arregloCitas[i]);
                    }
                } else {
                    for (int i = 0; i < arregloReservaciones.length; i++) {
                        output.println("#" + (i + 1) + " " + arregloReservaciones[i]);

                    }
                }

            case 7:
                break;
            default:
                output.println("Opción inválida");
                break;
        }
    }
}