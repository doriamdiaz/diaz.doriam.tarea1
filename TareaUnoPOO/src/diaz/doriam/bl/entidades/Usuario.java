package diaz.doriam.bl.entidades;

public class Usuario {
    private String nombreUsuario;
    private boolean activo;
    private String rol;

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Usuario(String nombreUsuario, boolean activo, String rol) {
        this.nombreUsuario = nombreUsuario;
        this.activo = activo;
        this.rol = rol;
    }

    public Usuario() {
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nombreUsuario='" + nombreUsuario + '\'' +
                ", activo=" + activo +
                ", rol='" + rol + '\'' +
                '}';
    }
}
