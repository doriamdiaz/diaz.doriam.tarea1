package diaz.doriam.bl.entidades;

public class Cita {
    private String mascotita;
    private String fecha;
    private String hora;
    private String observacion;

    public String getMascotita() {
        return this.mascotita;
    }

    public void setMascotita(String mascotita) {
        this.mascotita = mascotita;
    }

    public String getFecha() {
        return this.fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return this.hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getObservacion() {
        return this.observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Override
    public String toString() {
        return "La mascota " + this.mascotita + " tiene su cita el día : " + this.fecha + " a la hora : " + this.hora + " y se le atenderá para " + this.observacion + ".";
    }

    public Cita(String mascotita, String fecha, String hora, String observacion) {
        this.mascotita = mascotita;
        this.fecha = fecha;
        this.hora = hora;
        this.observacion = observacion;
    }

    public Cita() {
    }
}
