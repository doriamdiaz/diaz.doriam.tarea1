package diaz.doriam.bl.entidades;

public class Duenno {
    private String nombreDuenno;

    public String getNombreDuenno() {
        return nombreDuenno;
    }

    public void setNombreDuenno(String nombreDuenno) {
        this.nombreDuenno = nombreDuenno;
    }

    public Duenno(String nombreDuenno) {
        this.nombreDuenno = nombreDuenno;
    }

    public Duenno() {
    }

    @Override
    public String toString() {
        return "Duenno{" +
                "nombre='" + nombreDuenno + '\'' +
                '}';
    }
}
