package diaz.doriam.bl.entidades;

import java.util.Date;

public class Reservacion {
    private int numeroReservacion;
    private String nombreAnimal;
    private String diaEntrada;
    private String diaSalida;

    public int getNumeroReservacion() {
        return this.numeroReservacion;
    }

    public void setNumeroReservacion(int numeroReservacion) {
        this.numeroReservacion = numeroReservacion;
    }

    public String getNombreAnimal() {
        return this.nombreAnimal;
    }

    public void setNombreAnimal(String nombreAnimal) {
        this.nombreAnimal = nombreAnimal;
    }

    public String getDiaEntrada() {
        return this.diaEntrada;
    }

    public void setDiaEntrada(String diaEntrada) {
        this.diaEntrada = diaEntrada;
    }

    public String getDiaSalida() {
        return this.diaSalida;
    }

    public void setDiaSalida(String diaEntrada) {
        this.diaSalida = diaSalida;
    }

    @Override
    public String toString() {
        return "La mascota " + this.nombreAnimal + " tiene en el hotel el número de reservación: " + this.numeroReservacion + " para ingresar el día: " + this.diaEntrada + " y salir el día " + this.diaSalida + ".";
    }

    public Reservacion(int numeroReservacion, String nombreAnimal, String diaEntrada, String diaSalida) {
        this.numeroReservacion = numeroReservacion;
        this.nombreAnimal = nombreAnimal;
        this.diaEntrada = diaEntrada;
        this.diaSalida = diaSalida;
    }

    public Reservacion() {
    }
}
