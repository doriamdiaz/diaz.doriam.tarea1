package diaz.doriam.bl.entidades;

public class Mascota {
    private String nombreMascota;
    private Duenno nombreDuenno;
    private String foto;
    private float ranking;
    private String observaciones;


    public String getNombreMascota() {
        return this.nombreMascota;
    }

    public void setNombreMascota(String nombreMascota) {
        this.nombreMascota = nombreMascota;
    }

    public Duenno getNombreDuenno() {
        return nombreDuenno;
    }

    public void setNombreDuenno(Duenno nombreDuenno) {
        this.nombreDuenno = nombreDuenno;
    }


    public String getFoto() {
        return this.foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public float getRanking() {
        return this.ranking;
    }

    public void setRanking(float ranking) {
        this.ranking = ranking;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Mascota(String nombreMascota, Duenno nombreDuenno, String foto, float ranking, String observaciones) {
        this.nombreMascota = nombreMascota;
        this.nombreDuenno = nombreDuenno;
        this.foto = foto;
        this.ranking = ranking;
        this.observaciones = observaciones;
    }

    public Mascota() {
    }

    @Override
    public String toString() {
        return "Mascota{" +
                "nombreMascota='" + nombreMascota + '\'' +
                ", nombreDuenno=" + nombreDuenno +
                ", foto='" + foto + '\'' +
                ", ranking=" + ranking +
                ", observaciones='" + observaciones + '\'' +
                '}';
    }
}

