package diaz.doriam.bl.entidades;

public class Persona {
    private String nombreCompleto;
    private String cedula;
    private int telefono;
    private String direccion;

    public String getNombreCompleto() {
        return this.nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getCedula() {
        return this.cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public int getTelefono() {
        return this.telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Los datos de esta persona son: [nombreCompleto: " + this.nombreCompleto + ", cédula: " + this.cedula + ", teléfono: " + this.telefono + " y dirección " + this.direccion + "].";
    }

    public Persona(String nombreCompleto, String cedula, int telefono, String direccion) {
        this.nombreCompleto = nombreCompleto;
        this.cedula = cedula;
        this.telefono = telefono;
        this.direccion = direccion;
    }

    public Persona() {
    }
}
